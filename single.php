<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Simonas
 */
$post_categories = wp_get_post_categories( $post_id = get_the_ID() );
$cats = array();

foreach($post_categories as $c){
	$cat = get_category( $c );
	$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
}
get_header();
?>
<div class="content">

	<div class="container">
	
	<?php 
			if ( is_singular() ) :
			the_title( '<h1 class="entry-title simonas-post_header">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
	?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		
		while ( have_posts() ) : the_post();
			
			get_template_part( 'template-parts/simonas-post-page', get_post_type() );

		endwhile; // End of the loop.
		dynamic_sidebar('author');
		dynamic_sidebar('liked_article');
		?>

		<!--	HERE SHOULD GO SHARED BUTTOS	 -->
		<div class="next-story_mobile" style="margin-bottom:35px;">
			<div class="other-stories">
				<?php previous_post_link( '%link', 'PREVIOUS STORY');?>
				<div class="line next-story"></div>
				<?php next_post_link( '%link', 'NEXT STORY');?>
			</div>
			<div class="mobile_slicer"> </div>
			<div class="other-stories_headers">
				<?php previous_post_link('%link'); ?>
				<?php next_post_link('%link'); ?>
			</div>
		</div>

		<?php 
			if(comments_open() == 1){
				$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				?>
				
				<div id="fb-root"></div>
				<script async defer crossorigin="anonymous" src="https://connect.facebook.net/lt_LT/sdk.js#xfbml=1&version=v4.0"></script>
				<div class="fb-comments" data-href="<?php echo $actual_link; ?>" data-width="100%" data-numposts="5"></div>
				<?php
			}
		?>
<?php
$post_id = get_the_ID(); // or use the post id if you already have it
$category_object = get_the_category($post_id);
$category_name = $category_object[0]->name;
    // Get the ID of a given category
    $category_id = get_cat_ID( $category_name );

    // Get the URL of this category
    $category_link = get_category_link( $category_id );
?>


		

		</main><!-- #main -->

	</div><!-- #primary -->
	<aside id="secondary" class="widget-area">
		<aside id="secondary" class="widget-area">
			<div class="authorBox">
				<div class="authorBox-image">
				<?php echo get_avatar( get_the_author_meta('ID')); ?>
				</div>
				<div class="authorBox-name">
				<h2><?php the_author_meta('display_name', get_the_author_meta('ID')) ?></h2>
				<div class="authorBox-position">
				<h3><?php echo get_user_meta(get_the_author_meta('ID'), "status")[0] ; ?></h3>
				</div>
				<div class="authorBox-about">
					<?php the_author_meta('description', get_the_author_meta('ID')); ?>

				</div>
				<div class="authorBox-signature">
					<?php 
						echo get_user_meta(get_the_author_meta('ID'), "signature")[0];
					?>
				</div>
				</div>



			</div>

			<div class="authorBox-Box">
				<div class="authorBox-image">
				<?php echo get_avatar( get_the_author_meta('ID')); ?>
				</div>
				<div class="authorBox-name">
				<h2><?php the_author_meta('display_name', get_the_author_meta('ID')) ?></h2>
				<div class="authorBox-position">
				<h3><?php echo get_user_meta(get_the_author_meta('ID'), "status")[0] ; ?></h3>
				</div>
				<div class="authorBox-about">
					<?php the_author_meta('description', get_the_author_meta('ID')); ?>

				</div>
				<div class="authorBox-signature">
					<?php 
						echo get_user_meta(get_the_author_meta('ID'), "signature")[0];
					?>
				</div>
				</div>



			</div>
			
		</aside>
		<?php get_sidebar(); ?>
		<style>
			.custom-html-widget{
				display: none;
			}
		</style>
	</aside>
	
	<div class="next-story_mobile" style="margin-bottom:20px; margin-top: 31px; grid-column: 1/4;">
			<div class="other-stories">
			<a href="<?php echo esc_url( $category_link ); ?>" title="Category Name">RELATED STORIES</a>
				<div class="line next-story"></div>
				<a href="/">VIEW ALL</a>
			</div>
			<div class="mobile_slicer"> </div>
		</div>
	<?php
//for use in the loop, list 5 post titles related to first tag on current post
$tags = wp_get_post_tags($post->ID);
if ($tags) {
$first_tag = $tags[0]->term_id;
$args=array(
'tag__in' => array($first_tag),
'post__not_in' => array($post->ID),
'posts_per_page'=>3,
'caller_get_posts'=>1
);
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {


while ($my_query->have_posts()) : $my_query->the_post(); ?>
<div class="related_posts">
	<div class="related_posts-inner">
	<div class="related_posts-tags">
	<?php the_tags('', '<span class="spaceintags"></span>') ?>
	</div>
	<a class="related_posts-title" href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
	<div class="related_posts-author">
		<div class="author">
		<p><?php the_author(); ?> | <?php simonas_posted_on(); ?></p>
		</div>
		<div class="reading-time">
		<div class="readTime">
                      <i class="fa fa-clock-o" style="padding-right: 12px; font-size:21px;"></i>
                     <small>
                        <?php 
                        echo get_post_meta(get_the_ID())["_read_time_value_key"][0];
                        ?>
                        min read
                      </small>
                    </div>
		</div>
	</div>
	<div  onclick="linkToPost(this)" class="related_posts-image" link="<?php the_permalink(); ?>">
		<?php the_post_thumbnail() ?>
	</div>
	<script>
	const linkToPost = e =>{
		window.location.href = e.getAttribute('link');
	}
	</script>	
	</div>


</div>
 
<?php
endwhile;
}
wp_reset_query();
}
?>

	
	</div>
</div>
<style>
#secondary{
	margin-top: 0;
}
.custom-html-widget{
	display: none!important;
}
</style>




<?php

get_footer('new');
