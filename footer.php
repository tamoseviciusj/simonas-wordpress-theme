<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Simonas
 */

?>
<footer class="simonas-footer">
    <div class="footer_icons">
     <?php dynamic_sidebar( 'social_footer' ); ?>
    </div>

<?php dynamic_sidebar( 'instagram_posts' ); ?>
<div class="somonas-footer_copyrights">
    <div class="copyrights-inside copyright-text">
        <?php dynamic_sidebar( 'footer_copyrights' ); ?>
    </div>
    <div class="copyrights-inside footer-menu">
        <?php dynamic_sidebar( 'footer_navigation ' ); ?>
    </div>
</div>
</footer>


<?php wp_footer(); ?>

</body>
</html>

<script>

jQuery(".social-footer").live("click", function(){
    var link = jQuery(this).context.childNodes[2].getAttribute("href")
    window.location.href = link;
})
</script>