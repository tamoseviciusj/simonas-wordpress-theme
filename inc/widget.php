﻿﻿<?php 
/*
  @package simonas
  ================
    Widget Class
  ================  
*/

class Simonas_Header_Widget extends WP_Widget {

  /*
    Construct function 
  */
  public function __construct(){
    $widget_ops = array('className' => 'customClass', 'description' => 'Custom header post');
    
    parent::__construct('simonas_header', 'Simonas header', $widget_ops);
      
  }
  
  //Backend
  public function form($instance){
    echo '<p>No Options for this Widget </p>';
  }
  
  //front end
  public function widget($args, $instance ){
    
    $picture = esc_attr(get_option( 'profile_picture'));
  
    echo $args['before_widget'];
    
    ?>
      <nav id="site-navigation" class="main-navigation">
            <?php 
            the_custom_logo();
            ?>
            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'simonas' ); ?></button>
            <?php
            wp_nav_menu( array(
              'theme_location' => 'menu-1',
              'menu_id'        => 'primary-menu',
            ) );
        
            wp_nav_menu( array(
              'theme_location' => 'social',
              'menu_id'        => 'social',
            ) );    
                  ?>
            <div id="nav-search">
            <i data-search class="fa fa-search"></i>
                <?php
                    get_search_form();
                ?>
            </div>
                  
          </nav><!-- #site-navigation -->
    <?php
    echo $args['after_widget'];
  }
  
} 

add_action('widgets_init', function(){
  register_widget('Simonas_Header_Widget');
   }
);


class Simonas_Popular_Posts_Widget extends WP_Widget{
  
  public function __construct(){
      $widget_ops = array(
      'className' => 'simonas-popular-posts',
      'description' => 'Simonas Posts Widget',
      );
      
      parent::__construct('simonas_popular_posts', 'Simonas Popular Posts', $widget_ops);
      
  }
  
  public function form($instance){
    $title = ( !empty($instance['title']) ? $instance['title'] : 'Most popular posts' );  
    $tot = (!empty( $instance['tot']) ? absint($instance['tot']) : 4);
  
    $output = '<p>';
    $output .= '<label for="'. esc_attr($this->get_field_id('title')) . '">Title</label>';
    $output .= '<input type="text" class="widefat" 
                       id="' . esc_attr($this->get_field_id('title')) . '" 
                       name="' . esc_attr( $this->get_field_name('title')) . '" value="'. esc_attr($title) . '"'; 
    $output .= '</p>';
    
    $output .= '<p>';
        $output .= '<label for="'. esc_attr($this->get_field_id('tot')) . '">Number of Posts</label>';
        $output .= '<input type="number" class="widefat" 
                           id="' . esc_attr($this->get_field_id('tot')) . '" 
                           name="' . esc_attr( $this->get_field_name('tot')) . '" value="'. esc_attr($tot) . '"'; 
        $output .= '</p>';
      echo $output;
  }
  // Update widget
  public function update($new_instance, $old_instance){
    $instance = array();
    $instance['title'] = ( !empty( $new_instance['title'])  ? strip_tags($new_instance['title'] ): '');
    $instance['tot'] = ( !empty( $new_instance['tot'])  ? absint( strip_tags($new_instance['tot'])) : 0);
    
    return $instance;
    
  }
  
  //Front end display widget
  
  public function widget($args, $instance){
    
    $tot = absint($instance['tot']);
    
    $posts_args = array(
      'post_type' => 'post',
      'posts_per_page' => $tot,
      'meta_key' => 'post_views_count',
      'orderby' => 'meta_value_num',
      'order' => 'DESC'
    );
    
    $post_query = new WP_Query($posts_args);
      
     echo $args['before_widget'];
     if ( !empty($instance['title'])){
           echo  $args['before_title'] . apply_filters( 'widget_title' , $instance['title'] ) . $args['after_title']; 
         }
    if($post_query->have_posts()): ?> 
      
        <?php 
          while($post_query->have_posts()) : $post_query->the_post();?>
            <a href="<?php echo the_permalink(); ?> ">
              <section class="list-popular">
              <picture class="popular-image">
                <img  srcset="<?php the_post_thumbnail_url();?> 320w" sizes="(max-width: 320px) 280px" src="<?php the_post_thumbnail_url(); ?>" />
              </picture>
              <aside>
              <p>
              <?php 
                echo get_the_title();
              ?>
              </p>
              <small>
              <?php 
                the_author();
              ?>
              </small>
              </aside>
            </section></a>
          <?php
          endwhile;
        ?>
    
    <?php
    endif;
    echo $args['after_widget'];
  }
  
}

add_action('widgets_init', function(){
  register_widget('Simonas_Popular_Posts_Widget');
});


class Simonas_Recent_Posts extends WP_Widget{
      
      
    public function __construct(){
      $widget_ops = array(
        'className' => 'simonas-recent-posts',
        'description' => 'Simonas recent Widget',
      );
      
        parent::__construct('Simonas_Recent_Posts', 'Simonas Recent Posts', $widget_ops);
    }



    //frontend
   
    public function widget( $args, $instance ) {
    if ( ! isset( $args['widget_id'] ) ) {
      $args['widget_id'] = $this->id;
    }

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Simonas Recent Posts' );

    /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
    $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

    $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 3;

    if ( ! $number ) {
      $number = 3;
    }
    $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

    $r = new WP_Query(
      apply_filters(
        'widget_posts_args',
        array(
          'posts_per_page'      => $number,
          'no_found_rows'       => true,
          'post_status'         => 'publish',
          'ignore_sticky_posts' => true,
        ),
        $instance
      )
    );

    if ( ! $r->have_posts() ) {
      return;
    }
    ?>
    <?php echo $args['before_widget']; ?>
    <?php
    if ( $title ) {
      echo $args['before_title'] . $title . $args['after_title'];
    }
    ?>

 

      <?php foreach ( $r->posts as $recent_post ) : ?>
        <?php
        $post_title = get_the_title( $recent_post->ID );
        $title      = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
        ?>
     
        <a href="<?php echo the_permalink($recent_post->ID); ?> ">
              <section class="list-popular">
                <picture class="popular-image">
                  <img  srcset="<?php the_post_thumbnail_url();?> 320w" sizes="(max-width: 320px) 280px" src="<?php the_post_thumbnail_url(); ?>" />
                </picture>
                <aside>
                    <p>
                    <?php 
                      echo $title;
                    ?>
                    </p>
                    <small>
                    <?php 
                      the_author();
                    ?>
                    </small>
                </aside>
            </section>
          </a>
      <?php endforeach; ?>
    <?php
    echo $args['after_widget'];
  }


  //update

  public function update( $new_instance, $old_instance ) {
    $instance              = $old_instance;
    $instance['title']     = sanitize_text_field( $new_instance['title'] );
    $instance['number']    = (int) $new_instance['number'];
    return $instance;
  }

  //backend
  public function form( $instance ) {
    $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 3;
    ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

    <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
    <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>

    <p><input class="checkbox" type="checkbox"<?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
    <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label></p>
    <?php
  }

}

add_action('widgets_init', function() {
  register_widget('Simonas_Recent_Posts');
});



function simonas_save_post($postID){
  $metaKey = 'post_views_count';
  $views = get_post_meta($postID, $metaKey, true);
  $count = (empty( $views ) ? 0 : $views);
  $count++;

  echo $count;
  
  update_post_meta($postID, $metaKey, $count);

}

remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
