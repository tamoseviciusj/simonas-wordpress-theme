<?php 

class Simonas_Recent_Posts extends WP_Widget{
      
      
    public function __construct(){
      $widget_ops = array(
        'className' => 'simonas-recent-posts',
        'description' => 'Simonas recent Widget',
      );
      
        parent::__construct('Simonas_Recent_Posts', 'Simonas Recent Posts', $widget_ops);
    }



    //frontend
   
    public function widget( $args, $instance ) {
       		$post_categories = wp_get_post_categories( $post_id = get_the_ID() );
		$cats = array();
     
		foreach($post_categories as $c){
			$cat = get_category( $c );
			$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
		}
    if ( ! isset( $args['widget_id'] ) ) {
      $args['widget_id'] = $this->id;
    }

    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Simonas Recent Posts' );

    /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
    $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

    $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 2;

     if (has_post_thumbnail()) {
            $feautred_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_id() ) );          
          }


    if ( ! $number ) {
      $number = 3;
    }
    $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

    $r = new WP_Query(
      apply_filters(
        'widget_posts_args',
        array(
          'posts_per_page'      => $number,
          'no_found_rows'       => true,
          'post_status'         => 'publish',
          'ignore_sticky_posts' => true,
        ),
        $instance
      )
    );

    if ( ! $r->have_posts() ) {
      return;
    }
    ?>
<?php echo $args['before_widget']; 
     if ( !empty($instance['title']) && $args['name'] == "Sidebar"){
           echo  $args['before_title'] . apply_filters( 'widget_title' , $instance['title'] ) . $args['after_title']; 
     }  
      foreach ( $r->posts as $recent_post ):
        $post_title = get_the_title( $recent_post->ID );
        $title      = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
        if (has_post_thumbnail()) {
          $feautred_image = wp_get_attachment_url( get_post_thumbnail_id($recent_post->ID ) );          
        }

        if ($args['name'] == "Sidebar"):
        ?>
<a href="<?php echo the_permalink($recent_post->ID); ?> ">
    <section class="list-popular">
        <picture class="popular-image">

            <img src="<?php echo $feautred_image ?>" />
        </picture>
        <aside style="margin-left: 0;">
            <div>
                <p>
                    <?php 
                        echo $title;
                        
                      ?>
                </p>
                <small>

                    <?php $autorID = get_post_field( 'post_author', $recent_post->ID );
                     echo get_the_author_meta("display_name", $autorID)?>
                </small>
            </div>
        </aside>
        <style>
        .popular-image img {
            max-width: none;
        }
        </style>
    </section>
</a>
<?php 
            endif;
            if($args['name'] == 'Header'):
            $avatar = get_avatar( get_post_field( 'post_author', $post_id ));
            $status = get_user_meta(get_post_field( 'post_author', $post_id ), "status")[0];
            ?>
<a href="<?php echo the_permalink($recent_post->ID); ?> ">
    <section class="header-popular relative">
        <picture class="header-image">
            <img src="<?php echo $feautred_image ?>" />
        </picture>
        <aside class="rect absolute">
            <div class="rect-inside">
                <!-- IMPLAMENT ON MOST POPULAR ADD CLASS -->
                <h2 class="header-styling_category">
                    <?php foreach($cats as $c){echo $c['name'] . ' ';} ?>
                </h2>
                <h1 id="header-excerpt" class="the_excerpt">
                    <?php 
                        echo $title;
                      ?>
                </h1>
                <div class="avatar-section">
                    <?php echo $avatar; ?>
                    <div class="avatar-inner">
                        <small>
                            By
                            <?php 
                        global $post;
 
                        // modify the $post variable with the post data you want. Note that this variable must have this name!
                         
                        setup_postdata( $post ); 
                          the_author();
                          $userid = wp_get_current_user();

                          echo " | " . get_user_meta(get_post_field( 'post_author', $post_id ), "status")[0];
                        ?>

                        </small>
                        <?php 
                        echo get_the_date('Y-m-d');
                        ?>
                        </small>
                    </div>
                    <div class="readTime">
                        <i class="fa fa-clock-o" style="padding-right: 12px; font-size:21px;"></i>
                        <small>
                            <?php 
                        echo get_post_meta(get_the_ID())["_read_time_value_key"][0];
                        ?>
                            min read
                        </small>
                    </div>
                </div>
            </div>
        </aside>
    </section>
</a>
<?php
          break;
          endif;
       endforeach; ?>
<?php
    echo $args['after_widget'];
  }


  //update

  public function update( $new_instance, $old_instance ) {
    $instance              = $old_instance;
    $instance['title']     = sanitize_text_field( $new_instance['title'] );
    $instance['number']    = (int) $new_instance['number'];
    return $instance;
  }

  //backend
  public function form( $instance ) {
    $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 3;
    ?>
<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
        name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
    <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>"
        name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1"
        value="<?php echo $number; ?>" size="3" /></p>

<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?>
        id="<?php echo $this->get_field_id( 'show_date' ); ?>"
        name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
    <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label></p>
<?php
  }

}

add_action('widgets_init', function() {
  register_widget('Simonas_Recent_Posts');
});