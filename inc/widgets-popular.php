<?php 
class Simonas_Popular_Posts_Widget extends WP_Widget{
  
  public function __construct(){
      $widget_ops = array(
      'className' => 'simonas-popular-posts',
      'description' => 'Simonas Posts Widget',
      );
      
      parent::__construct('simonas_popular_posts', 'Simonas Popular Posts', $widget_ops);
      
  }
  
  public function form($instance){

    $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $tot = (!empty( $instance['tot']) ? absint($instance['tot']) : 1);
    $output = '<p>';
    $output .= '<label for="'. esc_attr($this->get_field_id('title')) . '">Title</label>';
    $output .= '<input type="text" class="widefat" 
                       id="' . esc_attr($this->get_field_id('title')) . '" 
                       name="' . esc_attr( $this->get_field_name('title')) . '" value="'. esc_attr($title) . '"'; 
    $output .= '</p>';
    
    $output .= '<p>';
        $output .= '<label for="'. esc_attr($this->get_field_id('tot')) . '">Number of Posts</label>';
        $output .= '<input type="number" class="widefat" 
                           id="' . esc_attr($this->get_field_id('tot')) . '" 
                           name="' . esc_attr( $this->get_field_name('tot')) . '" value="'. esc_attr($tot) . '"'; 
        $output .= '</p>';
      echo $output;
  }
  // Update widget
  public function update($new_instance, $old_instance){
    $instance = array();
    $instance['title'] = ( !empty( $new_instance['title'])  ? strip_tags($new_instance['title'] ): 'Most popular posts');
    $instance['tot'] = ( !empty( $new_instance['tot'])  ? absint( strip_tags($new_instance['tot'])) : 0);
    
    return $instance;
    
  }
  
  //Front end display widget
  
  public function widget($args, $instance){
    
    $tot = absint($instance['tot']);
    
    $userid = wp_get_current_user()->ID;
    $posts_args = array(
      'post_type' => 'post',
      'posts_per_page' => $args['name'] == "Header" ? 1 : $tot,
      'meta_key' => 'post_views_count',
      'orderby' => 'meta_value_num',
      'order' => 'DESC'
    );
    
    $post_query = new WP_Query($posts_args);
      
     
    echo $args['before_widget']; 
     if ( !empty($instance['title']) && $args['name'] == "Sidebar"){
           echo  $args['before_title'] . apply_filters( 'widget_title' , $instance['title'] ) . $args['after_title']; 
     }  
   
    if($post_query->have_posts()): ?> 
      
        <?php 
          while($post_query->have_posts()) : $post_query->the_post();?>
          <?php
            if ($args['name'] == 'Header') {
             $feautred_image = 'https://via.placeholder.com/1980x500';
            }else{
              $feautred_image = 'https://via.placeholder.com/150';  
            }
             
           if (has_post_thumbnail()) {
            $feautred_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_id() ) );          
          }

          ?>

            <a href="<?php echo the_permalink(); ?> ">
              <?php if ($args['name'] == "Sidebar"):?>
                <section class="list-popular">
                  <picture class="popular-image" style="width: auto;">
                    <img  src="<?php echo $feautred_image ?>" />
                  </picture>
                  
                  <aside style="margin-left: 0;">
                    <div> <!-- IMPLAMENT EXTRA DIV -->
                  <p>
                    <?php 
                     echo get_the_title();
                    ?>
                  </p>
                  <small>

                  <?php 
                    the_author();
                  ?>

                  </small>
                    </div>
                  </aside>
              </section></a>
          <?php
          endif;
              if($args['name'] == 'Header'): 
              $avatar = get_avatar( get_post_field( 'post_author', $post_id ));
              ?>
              <section class="header-popular relative">
                  <picture class="header-image">
                    <img  src="<?php echo $feautred_image ?>" />
                  </picture>
                  <aside class="rect absolute">
                    <div class="rect-inside">
                      <h2>
                      <?php 
                        echo get_the_title();
                      ?>
                      </h2>
                      <h1 id="header-excerpt" class="the_excerpt">
                        <?php 
                        $excerpt = get_the_excerpt();
                        $length = strlen(get_the_excerpt());
                          $out = $length > 100 ? substr($excerpt,0, 100)."" : $excerpt;
                          echo $out;
                        ?>
                      </h1>
                      <div class="avatar-section">
                        <?php echo $avatar; ?>
                      <div class="avatar-inner">
                       <small>
                        By  
                        <?php 
                          the_author();
                          $userid = wp_get_current_user();
                          echo  " | " . get_user_meta(get_post_field( 'post_author', $post_id ), "status")[0];
                        ?>

                        </small>
                        <small>
                          <?php 
                          echo get_the_date('Y-m-d');
                          ?>
                        </small>
                      </div>
                      <div class="readTime">
                        <i class="fa fa-clock-o" style="padding-right: 12px; font-size:21px;"></i>
                       <small>
                          <?php 
                          echo get_post_meta(get_the_ID())["_read_time_value_key"][0];
                          ?>
                          min read
                        </small>
                      </div>
                      </div>
                     
                    </div>
                  </aside>
              </section></a>
            <?php
            endif;
          endwhile;
        ?>
    
    <?php
    endif;
    echo $args['after_widget'];
  }
  
}

add_action('widgets_init', function(){
  register_widget('Simonas_Popular_Posts_Widget');
});
