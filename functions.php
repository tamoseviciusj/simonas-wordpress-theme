<?php
require get_template_directory() . '/inc/widgets-popular.php';
require get_template_directory() . '/inc/widgets-recent.php';
require get_template_directory() . '/inc/widgets-tags.php';
/**
 * Simonas functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Simonas
 */

if ( ! function_exists( 'simonas_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function simonas_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Simonas, use a find and replace
		 * to change 'simonas' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'simonas', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );  
		add_theme_support( 'html5', array('search-form'));

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'simonas' ),
			'social' => esc_html__('Social', 'simonas'),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'simonas_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'simonas_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function simonas_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'simonas_content_width', 640 );
}
add_action( 'after_setup_theme', 'simonas_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function simonas_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'simonas' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'simonas' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'simonas_widgets_init' );

function header_widgets_init() {
  register_sidebar(array(
    'name' => 'Header',
    'id' => 'header_post',
    'description' => esc_html__('Add the widgets you want to be in the header here', 'simonas'),
    'before_widget' => '<section id="masthead" class="site-header">',
    'after_widget' => '</section>',
    'before_title' => '<h2 class="widgets_title">',
    'after_title' => '</h2>',
  ));
}

add_action('widgets_init', 'header_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function simonas_scripts() {
	wp_enqueue_style( 'simonas-style', get_stylesheet_uri() );

	wp_enqueue_script( 'simonas-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'simonas-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'simonas_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function wmpudev_enqueue_icon_stylesheet() {
	wp_register_style( 'fontawesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
	wp_enqueue_style( 'fontawesome');
	
}	
add_action( 'wp_enqueue_scripts', 'wmpudev_enqueue_icon_stylesheet' );
function my_theme_scripts_function() {
  wp_enqueue_script("jquery");
  wp_enqueue_script( 'postscript',  get_template_directory_uri() . '/js/posts.js');

}

add_action('wp_enqueue_scripts','my_theme_scripts_function');

function simonas_save_post($postID){
  $metaKey = 'post_views_count';
  $views = get_post_meta($postID, $metaKey, true);
  $count = (empty( $views ) ? 0 : $views);
  $count++;

  echo $count;
  
  update_post_meta($postID, $metaKey, $count);

}


remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function darth_dump($array){
  echo "<pre>";
    var_dump($array); 
  echo "</pre>";
}

/*
	Custom user type
*/

function simonas_profile_fields( $user ) {

	$user = get_user_meta( $user->ID);
	if (!empty($user["status"][0] ) ) {
		$status = $user["status"][0];
	}
	if (!empty($user["signature"][0] ) ) {
		$signature = $user["signature"][0];
	}
	var_dump($signature);
	?>
	<h3><?php esc_html_e( 'Personal Information', 'crf' ); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="status"><?php esc_html_e( 'Your status', 'crf' ); ?></label></th>
			<td>
				<input type="text"
			       id="status"
			       name="status"
			       value="<?php echo $status; ?>"
			       class="regular-text"
				/>
			</td>
		</tr>
		<tr>

			<th><label for="status"><?php esc_html_e( 'Signature', 'crf' ); ?></label></th>
						<td>
					<input type="text" name="signature" 
							id="signature"
							value="<?php echo $signature; ?>"
							class="regular-text">
				</td>
		</tr>
	</table>
	<?php
}

add_action( 'show_user_profile', 'simonas_profile_fields' );
add_action( 'edit_user_profile', 'simonas_profile_fields' );

function simonas_update_profile_fields( $user_id ) {

	if ( ! current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}

	if ( ! empty( $_POST['status'] ) ) {
		update_user_meta( $user_id, 'status', $_POST['status']);
	}
	if ( ! empty( $_POST['signature'] ) ) {
		update_user_meta( $user_id, 'signature', $_POST['signature']);
	}
}


add_action( 'edit_user_profile_update', 'simonas_update_profile_fields' );
add_action( 'personal_options_update', 'simonas_update_profile_fields' );

function simonas_read_time_box(){
	add_meta_box("read", "Read Time", "read_time_box_callback", "post", 'side');
}


function read_time_box_callback($post){

	wp_nonce_field("simonas_read_time", "simonas_read_time_nonce");

	$value = get_post_meta($post->ID, "_read_time_value_key", true);

	echo "<label for='simonas_read_time_field'> Read time</label>";
	echo "<input type='number' id='simonas_read_time' name='simonas_read_time' value='" .  esc_attr( $value ) ."' />";

}

function simonas_read_time($postID){

	if (! isset($_POST['simonas_read_time_nonce'])) {
		return;
	}
	
	if (! wp_verify_nonce( $_POST['simonas_read_time_nonce'], 'simonas_read_time')) {
		return;
	}

	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return;
	}

	if (! current_user_can( 'edit_post', $postID)) {
		return;
	}

	if (! isset($_POST['simonas_read_time'])) {
		return;
	}


	 $my_data = sanitize_text_field($_POST['simonas_read_time']);

	update_post_meta($postID, "_read_time_value_key", $my_data);	 

}

add_action( "save_post", "simonas_read_time" );

add_action( "add_meta_boxes", "simonas_read_time_box");


function get_part_of_content($content, $amount=500){
	$length = strlen($content);
    $out = $length > $amount ? substr($content,0, $amount)."" : $content;
    echo $out;

}

function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Instagram posts',
		'id'            => 'instagram_posts',
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );
	register_sidebar( array(
		'name'          => 'Copyrights',
		'id'            => 'footer_copyrights',
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );
	register_sidebar( array(
		'name'          => 'Footer navigation',
		'id'            => 'footer_navigation',
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );
		register_sidebar( array(
		'name'          => 'Custom Html FORM',
		'id'            => 'custom_form',
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );
			register_sidebar( array(
		'name'          => 'Adds',
		'id'            => 'adds',
		'before_widget' => '<div class="custom-adds">',
		'after_widget'  => '</div>'
	) );
				register_sidebar( array(
		'name'          => 'All catagories',
		'id'            => 'all_catagories',
		'before_widget' => '<div class="allcatagories">',
		'after_widget'  => '</div>'
	) );
				register_sidebar( array(
		'name'          => 'social',
		'id'            => 'social',
		'before_widget' => '<span class="icon-item">',
		'after_widget'  => '</span>'
				));
	register_sidebar( array(
		'name'          => 'Author',
		'id'            => 'author',
		'before_widget' => '<div class="author-ofpost">',
		'after_widget'  => '</div>'
	) );
	register_sidebar( array(
		'name'          => 'Footer social',
		'id'            => 'social_footer',
		'before_widget' => '<div class="social-footer">',
		'after_widget'  => '</div>'
	) );
	register_sidebar( array(
		'name'          => 'Single Post Footer HTML',
		'id'            => 'single_post_footer',
		'before_widget' => '<div class="single-post-footer">',
		'after_widget'  => '</div>'
	) );

	register_sidebar( array(
		'name'          => 'Mobile side bar main page',
		'id'            => 'mobile-side-bar',
		'before_widget' => '<div class="mobile_sideBar">',
		'after_widget'  => '</div>'
	) );


}
add_action( 'widgets_init', 'arphabet_widgets_init' );

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
	} else {
		echo $excerpt;
	}
}


