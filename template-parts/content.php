<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Simonas
 */

 		$post_categories = wp_get_post_categories( $post_id = get_the_ID() );
		$cats = array();
     
		foreach($post_categories as $c){
			$cat = get_category( $c );
			$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug, 'id' => $cat->cat_ID );
		}
		
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php $postURL =  get_post_permalink(); ?>
<a style="
    text-decoration: none;
" href="<?php echo $postURL; ?>"><p  class="front-page-expert front-page-exper_show"><?php  get_part_of_content( wp_strip_all_tags( get_the_title(), true ), 96); ?></p></a>

	<header class="entry-header front-posts">
		<?php


		if ( 'post' === get_post_type() ) :
			
			?>
			<div class="front-page-categories">
			<?php foreach($cats as $c){
				?>
				<a href="<?php echo get_category_link($c['id']) ?>"><?php {echo $c['name'] . ' ';} ?> </a>
				<?php
			}
				 ?>
			</div>	

		<?php endif; ?>
			<p class="front-page-expert"><?php  get_part_of_content( wp_strip_all_tags( get_the_title(), true ), 96); ?></p>

	<?php simonas_post_thumbnail(); ?>
	<div class='rect absolute'>
		
	</div>
	</header><!-- .entry-header -->
	<div class="front-content">
		<div class="front-content-header">
			
		<div class="avatar-section avatar-section_text">
					  <?php 
					  $avatar = get_avatar( get_the_author_meta('ID'));
					  echo $avatar; ?>
                    <div class="avatar-inner">
                     <small>
                      By  
                      <?php 
                        the_author_meta('display_name', get_the_author_meta('ID'));
                        $userid = wp_get_current_user();?>
                        <span style="padding:0px 5px;">
                          <?php
                          echo  " | " . get_user_meta(get_the_author_meta('ID'), "status")[0];
                          ?>
                        </span>
                        <?php
                        $userid = wp_get_current_user();
                        ?>

                      </small>
                      <small>
                        <?php 
                        echo get_the_date('Y-m-d');
                        ?>
                      </small>
                    </div>
                    <div class="readTime">
                      <i class="fa fa-clock-o" style="padding-right: 12px; font-size:21px;"></i>
                     <small>
                        <?php 
                        echo get_post_meta(get_the_ID())["_read_time_value_key"][0];
                        ?>
                        min read
                      </small>
                    </div>
					</div>
					<div class="content-margin">
						<p>
							<?php
							get_part_of_content( wp_strip_all_tags(get_the_content()));
							?>
							</p>
					</div>
					<div onclick="window.location.href = this.getAttribute('url');" url="<?php echo $postURL ?>" class="content-margin content-margin-show">
		<?php
		get_part_of_content( wp_strip_all_tags(get_the_content()), 210);
		?>
					</div>

		<br>
		<a class="front-read-more" href="<?php echo get_post_permalink(get_the_ID()) ?>">Read more <i class="fa fa-arrow-right"></i></a>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

