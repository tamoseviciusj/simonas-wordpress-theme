<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Simonas
 */

 		$post_categories = wp_get_post_categories( $post_id = get_the_ID() );
		$cats = array();
     
		foreach($post_categories as $c){
			$cat = get_category( $c );
			$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
		}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<header class="entry-header single-post_entryHeader">
		<?php
		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
                <span class="post-categories"><?php foreach($cats as $c){echo $c['name'] . ' ';} ?></span>
                <div class="dateandtime">
                <div class="readTime">
                      <i class="fa fa-clock-o" style="padding-right: 12px; font-size:21px;"></i>
                     <small>
                        <?php 
                        echo get_post_meta(get_the_ID())["_read_time_value_key"][0];
                        ?>
                        min read
                      </small>
                    </div>
                <?php
                

				simonas_posted_on();
				
                ?>
                </div>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<!--<p class="entry-expert"><?php //echo wp_strip_all_tags( the_excerpt_max_charlength(500), true ); ?></p> -->

	<?php// simonas_post_thumbnail(); ?>

	<div class="entry-content paragraph-styling">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'simonas' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'simonas' ),
			'after'  => '</div>',
		) );

		?>

		
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->

