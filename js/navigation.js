/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function() {
  var buttonsOne = document.getElementsByClassName("menu-toggle")[0];
  var buttonTwo = document.getElementsByClassName("menu-toggle")[1];

  buttonsOne.onclick = () => {
    dealWithNavigationClicks();
  };
  buttonTwo.onclick = () => {
    dealWithNavigationClicks();
  };
  var toggleMenu = false;
  var headerText = document.getElementById("header-text");
  var navigation = document.getElementsByClassName("mobile");
  console.log(navigation[0]);
  const dealWithNavigationClicks = () => {
    if (toggleMenu) {
      headerText.style.display = "none";
      navigation[0].style.top = 0;
      navigation[0].style.height = "100%";
    } else {
      headerText.style.display = "flex";
      navigation[0].style.top = "57px";
      navigation[0].style.height = "auto";
    }

    toggleMenu = !toggleMenu;
  };
  dealWithNavigationClicks();
  var container, button, menu, links, i, len;
  var search = document.querySelector("#nav-search");
  var searchInput = document.querySelector("#nav-search .search-form");
  var searchField = document.querySelector(
    "#nav-search .search-form .search-field"
  );
  var mobileSearch = document.querySelector(".search-field");
  var mobileHeader = document.querySelector(".mobile");
  var siteNav = document.querySelector("#site-navigation");

  var open = document.querySelector("#btn-open");
  var opensvg = document.querySelector("#opensvg");
  var close = document.querySelector("#btn-close");
  var closesvg = document.querySelector("#closesvg");

  var toggleBtn = document.querySelectorAll(".menu-toggle");
  var navItems = document.querySelector("#primary-menu li");
  var logo = document.querySelector(".custom-logo");
  var submitBtn = document.querySelector("#submit-btn");
  submitBtn.style.display = "none";
  search.addEventListener(
    "click",
    function(evt) {
      searchInput.style.opacity = "1";
      searchInput.style.display = "flex";
      searchInput.style.zIndex = "19";
      searchInput.style.border = "1px solid #071d2c";
      document.querySelector("#nav-search input[type='submit']").style.display =
        "none";
      searchInput.append(submitBtn);
      submitBtn.style.display = "block";
      searchField.focus();
    },
    true
  );

  document.querySelector("button > svg").addEventListener("click", function(e) {
    e.stopPropagation();
    e.preventDefault();
  });

  toggleBtn.forEach(function(btn) {
    btn.addEventListener(
      "click",
      function(evt) {
        if (evt.target.id == "btn-open") {
          open.style.display = "none";
          opensvg.style.display = "none";
          logo.classList.add("invert-color");
          close.style.display = "block";
          closesvg.style.display = "block";
        } else {
          open.style.display = "block";
          opensvg.style.display = "block";
          closesvg.style.display = "none";
          close.style.display = "none";
          logo.classList.remove("invert-color");
        }

        if (siteNav.classList.contains("none")) {
          siteNav.style.display = "block";
          siteNav.style.opacity = "1";
          mobileHeader.style.height = "100%";
          mobileHeader.style.backgroundColor = "#0B263A";
          siteNav.classList.remove("none");
        } else {
          mobileHeader.style.height = "auto";
          mobileHeader.style.backgroundColor = "#f7f6f6";
          siteNav.style.display = "none";
          siteNav.style.opacity = "0";
          siteNav.classList.add("none");
        }
      },
      true
    );
  });

  window.addEventListener(
    "click",
    function(evt) {
      if (!searchInput.contains(evt.target)) {
        searchInput.style.display = "none";
      }
    },
    true
  );

  container = document.getElementById("site-navigation");
  if (!container) {
    return;
  }

  button = container.getElementsByTagName("button")[0];
  if ("undefined" === typeof button) {
    return;
  }

  menu = container.getElementsByTagName("ul")[0];

  // Hide menu toggle button if menu is empty and return early.
  if ("undefined" === typeof menu) {
    button.style.display = "none";
    return;
  }

  menu.setAttribute("aria-expanded", "false");
  if (-1 === menu.className.indexOf("nav-menu")) {
    menu.className += " nav-menu";
  }

  button.onclick = function() {
    if (-1 !== container.className.indexOf("toggled")) {
      container.className = container.className.replace(" toggled", "");
      button.setAttribute("aria-expanded", "false");
      menu.setAttribute("aria-expanded", "false");
    } else {
      container.className += " toggled";
      button.setAttribute("aria-expanded", "true");
      menu.setAttribute("aria-expanded", "true");
    }
  };

  // Get all the link elements within the menu.
  links = menu.getElementsByTagName("a");

  // Each time a menu link is focused or blurred, toggle focus.
  for (i = 0, len = links.length; i < len; i++) {
    links[i].addEventListener("focus", toggleFocus, true);
    links[i].addEventListener("blur", toggleFocus, true);
  }

  /**
   * Sets or removes .focus class on an element.
   */
  function toggleFocus() {
    var self = this;

    // Move up through the ancestors of the current link until we hit .nav-menu.
    while (-1 === self.className.indexOf("nav-menu")) {
      // On li elements toggle the class .focus.
      if ("li" === self.tagName.toLowerCase()) {
        if (-1 !== self.className.indexOf("focus")) {
          self.className = self.className.replace(" focus", "");
        } else {
          self.className += " focus";
        }
      }

      self = self.parentElement;
    }
  }

  /**
   * Toggles `focus` class to allow submenu access on tablets.
   */
  (function(container) {
    var touchStartFn,
      i,
      parentLink = container.querySelectorAll(
        ".menu-item-has-children > a, .page_item_has_children > a"
      );

    if ("ontouchstart" in window) {
      touchStartFn = function(e) {
        var menuItem = this.parentNode,
          i;

        if (!menuItem.classList.contains("focus")) {
          e.preventDefault();
          for (i = 0; i < menuItem.parentNode.children.length; ++i) {
            if (menuItem === menuItem.parentNode.children[i]) {
              continue;
            }
            menuItem.parentNode.children[i].classList.remove("focus");
          }
          menuItem.classList.add("focus");
        } else {
          menuItem.classList.remove("focus");
        }
      };

      for (i = 0; i < parentLink.length; ++i) {
        parentLink[i].addEventListener("touchstart", touchStartFn, false);
      }
    }
  })(container);
})();
