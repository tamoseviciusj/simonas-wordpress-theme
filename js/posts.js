(function($) {
  $(document).ready(function() {
    if (screen.width < 768) {
      $("#secondary").prependTo(".sidebar-mobile");
    }
    $(window).resize(function() {
      if (screen.width < 768) {
        $("#secondary").prependTo(".sidebar-mobile");
      }
    });

    var post = document.getElementsByClassName("post");
    var countChildNodes = post[0].getElementsByClassName("entry-content")[0]
      .childNodes.length;
    var newItem = document.createElement("h4");
    var newUserbox = document.getElementsByClassName("authorBox")[0];
    var textnode = document.createTextNode("liked article? Share it"); // Create a text node
    newItem.appendChild(textnode); // Append the text to <li>

    //var list = document.getElementById("myList");    // Get the <ul> element to insert a new node
    post[0]
      .getElementsByClassName("entry-content")[0]
      .insertBefore(
        newItem,
        post[0].getElementsByClassName("entry-content")[0].childNodes[
          countChildNodes - 2
        ]
      );
    post[0]
      .getElementsByClassName("entry-content")[0]
      .insertBefore(
        newUserbox,
        post[0].getElementsByClassName("entry-content")[0].childNodes[
          countChildNodes - 2
        ]
      );
  });
})(jQuery);
