  <?php
  /**
  * The header for our theme
  *
  * This is the template that displays all of the <head> section and everything up until <div id="content">
  *
  * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
  *
  * @package Simonas
  */
  
  ?>
  <!doctype html>
  <html <?php language_attributes(); ?>>

  <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="profile" href="https://gmpg.org/xfn/11">

      <?php wp_head(); ?>
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>

  <body <?php body_class(); ?>>

      <div id="page" class="site">
          <div id="header-text">
              <i id="banner-text"> <span>Want to start a profitable education business today?</span> <a
                      href="http://internetideas.com/">Follow this 5 minute tutorial <svg
                          xmlns="http://www.w3.org/2000/svg" width="15px" viewBox="0 0 31.49 31.49">
                          <path
                              d="M21.205 5.007a1.112 1.112 0 0 0-1.587 0 1.12 1.12 0 0 0 0 1.571l8.047 8.047H1.111A1.106 1.106 0 0 0 0 15.737c0 .619.492 1.127 1.111 1.127h26.554l-8.047 8.032c-.429.444-.429 1.159 0 1.587a1.112 1.112 0 0 0 1.587 0l9.952-9.952a1.093 1.093 0 0 0 0-1.571l-9.952-9.953z"
                              fill="#FFF" /></svg></a></i>

          </div>
          <header class="mobile">
              <div class="relative mobile-header">
                  <?php 
            the_custom_logo();
            ?>
                  <button id="btn-open" class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                      <svg id="opensvg" width="50" height="50" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                          <path
                              d="M491.32 235.32H20.68a20.68 20.68 0 1 0 0 41.36h470.64a20.68 20.68 0 1 0 0-41.36zM491.32 78.44H20.68a20.68 20.68 0 0 0 0 41.36h470.64a20.68 20.68 0 1 0 0-41.36zM491.32 392.2H20.68a20.68 20.68 0 1 0 0 41.36h470.64a20.68 20.68 0 1 0 0-41.36z" />
                      </svg>
                  </button>
                  <button id="btn-close" class="menu-toggle hidden" aria-controls="primary-menu" aria-expanded="false">
                      <svg id="closesvg" width="50" height="50" fill="#FFF" xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512">
                          <path
                              d="M505.94 6.06a20.68 20.68 0 0 0-29.25 0L6.06 476.69a20.68 20.68 0 1 0 29.25 29.25L505.94 35.31a20.68 20.68 0 0 0 0-29.25z" />
                          <path
                              d="M505.94 476.7L35.31 6.05A20.68 20.68 0 1 0 6.06 35.3l470.63 470.63a20.62 20.62 0 0 0 14.63 6.06 20.69 20.69 0 0 0 14.62-35.3z" />
                      </svg>
                  </button>

              </div>
              <nav id="site-navigation" class="main-navigation none">
                  <span id="logo">
                      <?php 
              the_custom_logo();
              ?>
                  </span>
                  <div id="mobile-search" class="relative" data-search>
                      <?php
                          get_search_form();
                      ?>
                      <svg xmlns="http://www.w3.org/2000/svg" fill="#FFF" id="search-icon-head" width="50" height="50"
                          viewBox="0 0 451 451">
                          <path
                              d="M447.05 428l-109.6-109.6c29.4-33.8 47.2-77.9 47.2-126.1C384.65 86.2 298.35 0 192.35 0 86.25 0 .05 86.3.05 192.3s86.3 192.3 192.3 192.3c48.2 0 92.3-17.8 126.1-47.2L428.05 447c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4c5.2-5.2 5.2-13.8 0-19zM26.95 192.3c0-91.2 74.2-165.3 165.3-165.3 91.2 0 165.3 74.2 165.3 165.3s-74.1 165.4-165.3 165.4c-91.1 0-165.3-74.2-165.3-165.4z" />
                          </svg>

                  </div>
                  <?php
            wp_nav_menu( array(
              'theme_location' => 'menu-1',
              'menu_id'        => 'primary-menu',
            ) );
            ?>
                  <div class="mobile_nav-footer">
                      <div class="social-icons">
                          <?php
                dynamic_sidebar( 'social' );
                ?>
                          <div id="nav-search" data-search>
                              <i data-search class="fa fa-search"></i>
                              <?php
                          get_search_form();
                      ?>
                              <button id="submit-btn" type="submit">
                                  <i class="fa fa-search"></i>
                              </button>
                          </div>

                      </div>
                      <small class="mobile-copyright">
                          © 2017 – 2019 INTERNET IDEAS PUBLISHING Limited
                      </small>
                  </div>
              </nav><!-- #site-navigation -->
          </header>
          <a class="skip-link screen-reader-text"
              href="#content"><?php esc_html_e( 'Skip to content', 'simonas' ); ?></a>
          <div id="content">